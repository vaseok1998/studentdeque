package com.endava.internship;


import java.util.*;

public class ArrayDeque<T> implements Deque<T> {
    private final static int DEFAULT_ARRAYDEQUE_CAPACITY = 8;
    private final static int DEFAULT_LOAD_FACTOR = 2;
    private final int loadFactor;
    private Object[] elements;
    private int head = -1;
    private int tail = -1;
    private int numbOfElements;

    public ArrayDeque() {
        elements = new Object[DEFAULT_ARRAYDEQUE_CAPACITY];
        this.loadFactor = DEFAULT_LOAD_FACTOR;
    }

    public ArrayDeque(Collection<? extends T> c) {
        elements = new Object[c.size()];
        addAll(c);
        this.loadFactor = DEFAULT_LOAD_FACTOR;
    }

    public ArrayDeque(int capacity, int loadFactor) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("Invalid Capacity! It must be greater than 0.");
        }
        elements = new Object[capacity];
        this.loadFactor = loadFactor;
    }

    public void printElements() {
        if (isEmpty()) {
            System.out.println("ArrayDeque is empty.");
        } else {
            for (int i = head; i <= tail; i++) {
                System.out.println(elements[i].toString());
            }
        }
    }

    public int getHead() {
        return head;
    }

    public int getTail() {
        return tail;
    }

    public int getCapacity() {
        return elements.length;
    }

    private void resize() {
        int newCapacity = elements.length * loadFactor;
        Object[] newStList = new Object[newCapacity];
        System.arraycopy(elements, head, newStList, head, numbOfElements);
        elements = newStList;
    }

    private void remove(int index, int elementsOmitted) {
        Object[] copy = new Object[elements.length];
        elements[index] = null;
        numbOfElements--;
        if (index == tail) {
            tail--;
        } else if (index == head) {
            head++;
        } else if (index > head) {
            System.arraycopy(elements, head, copy, head + 1, elementsOmitted);
            head++;
            System.arraycopy(elements, index + 1, copy, index + 1, numbOfElements - elementsOmitted);
            elements = copy;
        }
    }

    @Override
    public void addFirst(T t) {
        Objects.requireNonNull(t, "Element should not be null.");
        if (isEmpty()) {
            head = 0;
            tail = 0;
            elements[head] = t;
        } else if (head == 0) {
            if (numbOfElements == elements.length) {
                resize();
            }
            tail++;
            Object[] newStList = new Object[elements.length];
            System.arraycopy(elements, head, newStList, head + 1, numbOfElements);
            elements = newStList;
            elements[head] = t;
        } else {
            head--;
            elements[head] = t;
        }
        numbOfElements++;
    }

    @Override
    public void addLast(T t) {
        Objects.requireNonNull(t, "Element should not be null.");
        if (isEmpty()) {
            head = 0;
            tail = 0;
            elements[head] = t;
        } else {
            if (tail == elements.length - 1) {
                resize();
            }
            tail++;
            elements[tail] = t;
        }
        numbOfElements++;
    }

    @Override
    public boolean offerFirst(T t) {
        addFirst(t);
        return true;
    }

    @Override
    public boolean offerLast(T t) {
        addLast(t);
        return true;
    }

    @Override
    public T removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return pollFirst();
    }

    @Override
    public T removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return pollLast();
    }

    @Override
    public T pollFirst() {
        if (isEmpty()) {
            return null;
        }
        T t = (T) elements[head];
        elements[head] = null;
        head++;
        numbOfElements--;
        return t;
    }

    @Override
    public T pollLast() {
        if (isEmpty()) {
            return null;
        }
        T t = (T) elements[tail];
        elements[tail] = null;
        tail--;
        numbOfElements--;
        return t;
    }

    @Override
    public T getFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return (T) elements[head];
    }

    @Override
    public T getLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return (T) elements[tail];
    }

    @Override
    public T peekFirst() {
        if (isEmpty()) {
            return null;
        }
        return (T) elements[head];
    }

    @Override
    public T peekLast() {
        if (isEmpty()) {
            return null;
        }
        return (T) elements[tail];
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
        int copyElements = 0;
        for (int i = head; i <= tail; i++) {
            if (elements[i].equals(o)) {
                remove(i, copyElements);
                return true;
            }
            copyElements++;
        }
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
        int copyElements = 0;
        int lastIndex = -1;
        for (int i = head; i <= tail; i++) {
            if (elements[i].equals(o)) {
                lastIndex = i;
            }
        }
        if (lastIndex >= head && lastIndex <= tail) {
            copyElements = numbOfElements - (tail - lastIndex) - 1;
            remove(lastIndex, copyElements);
            return true;
        }
        return false;
    }

    @Override
    public boolean add(T t) {
        addLast(t);
        return true;
    }

    @Override
    public boolean offer(T t) {
        return offerLast(t);
    }

    @Override
    public T remove() {
        return removeFirst();
    }

    @Override
    public T poll() {
        return pollFirst();
    }

    @Override
    public T element() {
        return getFirst();
    }

    @Override
    public T peek() {
        return peekFirst();
    }

    @Override
    public void push(T t) {
        addFirst(t);
    }

    @Override
    public T pop() {
        return removeFirst();
    }

    @Override
    public boolean remove(Object o) {
        return removeFirstOccurrence(o);
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
        for (int i = head; i <= tail; i++) {
            if (elements[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return numbOfElements;
    }

    @Override
    public Iterator<T> iterator() {
        return new ForwardIter();
    }

    @Override
    public Iterator<T> descendingIterator() {
        return new DescendingIter();
    }

    @Override
    public boolean isEmpty() {
        return numbOfElements == 0;
    }

    @Override
    public Object[] toArray() {
        Object[] copy = new Object[numbOfElements];
        System.arraycopy(elements, head, copy, 0, numbOfElements);
        return copy;
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        if (ts == null) {
            throw new NullPointerException();
        }
        if (ts.length < numbOfElements) {
            Object[] copy = new Object[elements.length];
            ts = (T[]) copy;
            ts[numbOfElements] = null;
        }
        System.arraycopy(elements, head, ts, 0, numbOfElements);
        return ts;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        for (Object obj : collection) {
            if (!contains(obj)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        if (collection.size() == 0) {
            return false;
        }
        for (Object t : collection) {
            add((T) t);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        int changes = 0;
        int copyElements = 0;
        for (int i = head; i <= tail; i++) {
            if (collection.contains(elements[i])) {
                remove(i, copyElements);
                changes++;
                copyElements--;
            }
            copyElements++;
        }
        return changes != 0;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        int changes = 0;
        int copyElements = 0;
        for (int i = head; i <= tail; i++) {
            if (!(collection.contains(elements[i]))) {
                remove(i, copyElements);
                changes++;
                copyElements = 0;
            }
            copyElements++;
        }
        return changes != 0;
    }

    @Override
    public void clear() {
        while (tail >= head) {
            elements[tail] = null;
            tail--;
            numbOfElements--;
        }
        tail = -1;
        head = -1;
    }

    class ForwardIter implements Iterator<T> {
        int currentPosition = head;

        @Override
        public boolean hasNext() {
            return currentPosition <= tail;
        }

        @Override
        public T next() {
            Object t = elements[currentPosition];
            currentPosition++;
            return (T) t;
        }
    }

    class DescendingIter implements Iterator<T> {
        int currentPosition = tail;


        @Override
        public boolean hasNext() {
            return currentPosition >= head;
        }

        @Override
        public T next() {
            Object t = elements[currentPosition];
            currentPosition--;
            return (T) t;
        }
    }
}


