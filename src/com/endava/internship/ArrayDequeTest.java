package com.endava.internship;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ArrayDequeTest {
    ArrayDeque<Student> arrayDeque;
    Student st = new Student("Vasea", LocalDate.now(), "Vasea");
    Student st1 = new Student("Vasea1", LocalDate.now(), "Vasea1");
    Student st2 = new Student("Vasea2", LocalDate.now(), "Vasea2");
    Student st3 = new Student("Vasea3", LocalDate.now(), "Vasea3");
    Student st4 = new Student("Vasea4", LocalDate.now(), "Vasea4");
    Student st5 = new Student("Vasea5", LocalDate.now(), "Vasea5");
    Student st6 = new Student("Vasea6", LocalDate.now(), "Vasea6");
    Student st7 = new Student("Vasea7", LocalDate.now(), "Vasea7");

    @BeforeEach
    public void setUp() {
        arrayDeque = new ArrayDeque<>(); // defaultLength = 8   // st6, st4, st4, st2, st1, st5, st7, st6
        arrayDeque.add(st1);                                    //  0    1    2    3    4    5    6    7   - size = 8;
        arrayDeque.addFirst(st2);
        arrayDeque.addFirst(st4);
        arrayDeque.addFirst(st4);
        arrayDeque.addLast(st5);
        arrayDeque.addLast(st7);
        arrayDeque.add(st6);
        arrayDeque.addFirst(st6);
    }

    @Test
    void shouldChangeHeadPositionAfterRemoveFirstMethodWasCalled() {
        arrayDeque.add(st);
        arrayDeque.add(st1);
        arrayDeque.removeFirst();
        arrayDeque.removeFirst();

        assertEquals(2, arrayDeque.getHead());
    }

    @Test
    void shouldDecrementTailAfterRemoveLastMethodWasCalled() {
        int previousIndexOfTail = arrayDeque.getTail();
        arrayDeque.removeLast();

        assertNotEquals(previousIndexOfTail, arrayDeque.getTail());
    }

    @Test
    void shouldIncrementTheCapacityAfterAddingAnElement() {
        int actualCapacity = arrayDeque.getCapacity();
        arrayDeque.add(st);

        assertEquals(actualCapacity * 2, arrayDeque.getCapacity());
    }

    @Test
    void shouldThrowExceptionIfRemoveIsCalledAfterClearMethod() {
        arrayDeque.clear();
        assertThrows(NoSuchElementException.class, () -> arrayDeque.remove());
    }

    @Test
    void shouldChangeElementFromHeadPosition() {
        Student student = arrayDeque.getFirst();
        arrayDeque.addFirst(st7);

        assertNotEquals(student, arrayDeque.getFirst());
    }

    @Test
    void shouldChangeElementFromTailPosition() {
        Student student = arrayDeque.getLast();
        arrayDeque.addLast(st2);
        arrayDeque.addLast(st3);
        arrayDeque.addLast(st5);

        assertNotEquals(student, arrayDeque.getLast());
    }

    @Test
    void shouldThrowANullWhenCallingOfferFirstWithANullParameter() {

        assertThrows(NullPointerException.class, () -> arrayDeque.offerFirst(null));
    }

    @Test
    void shouldAddAnElementEvenIfTheCapacityIsAchievedWithOfferLast() {
        assertAll(
                () -> assertEquals(arrayDeque.getCapacity(), arrayDeque.size()),
                () -> assertEquals(true, arrayDeque.offerLast(st2)),
                () -> assertEquals(st2, arrayDeque.getLast())
        );
    }

    @Test
    void shouldRemoveFirstThreeElementsFromTheArrayDeque() {
        int initialHeadPosition = arrayDeque.getHead();
        arrayDeque.removeFirst();
        arrayDeque.removeFirst();
        arrayDeque.removeFirst();

        assertAll(
                () -> assertEquals(initialHeadPosition + 3, arrayDeque.getHead()),
                () -> assertEquals(st2, arrayDeque.getFirst())
        );
    }

    @Test
    void shouldDecrementTailTwoTimesAfterCallingRemoveLastTwoTimes() {
        int initialTailIndex = arrayDeque.getTail();
        arrayDeque.removeLast();
        arrayDeque.removeLast();

        assertEquals(initialTailIndex - 2, arrayDeque.getTail());
    }

    @Test
    void shouldReturnNullIfArrayDequeIsEmptyWhenCallingPollFirst() {
        arrayDeque.clear();

        assertAll(
                () -> assertTrue(arrayDeque.isEmpty()),
                () -> assertEquals(null, arrayDeque.pollFirst())
        );
    }

    @Test
    void shouldRemoveAndReturnLastElementWhenCallingPollLast() {
        assertEquals(st6, arrayDeque.pollLast());
    }

    @Test
    void shouldReturnFirstElementIfArrayIsNotEmpty() {
        assertAll(
                () -> assertTrue(!arrayDeque.isEmpty()),
                () -> assertEquals(st6, arrayDeque.getFirst())
        );
    }

    @Test
    void shouldReturnLastElementIfArrayIsNotEmpty() {
        assertAll(
                () -> assertTrue(!arrayDeque.isEmpty()),
                () -> assertEquals(st6, arrayDeque.getLast())
        );
    }

    @Test
    void shouldReturnNullIfArrayIsEmpty() {
        arrayDeque.clear();

        assertEquals(null, arrayDeque.peekFirst());
    }

    @Test
    void shouldReturnLastElementIfNotEmpty() {
        assertFalse(arrayDeque.isEmpty());
        assertEquals(st6, arrayDeque.peekLast());
    }

    @Test
    void shouldRemoveFirstOccurrenceOfTheElement() {
        assertTrue(arrayDeque.removeFirstOccurrence(st6));
    }

    @Test
    void shouldThrowExceptionIfGivenParameterIsNull() {
        assertThrows(NullPointerException.class, () -> arrayDeque.removeFirstOccurrence(null));
    }

    @Test
    void shouldIncrementTailAndAddANewElementOnThatPosition() {
        int initialTailPosition = arrayDeque.getTail();

        assertTrue(arrayDeque.add(st2));
        assertEquals(initialTailPosition + 1, arrayDeque.getTail());
    }

    @Test
    void shouldReturnTrueIfElementWasAdded() {
        assertTrue(arrayDeque.offer(st1));
    }

    @Test
    void shouldRemoveFirstElementAndReturnIt() {
        Student student = arrayDeque.getFirst();

        assertEquals(student, arrayDeque.remove());
    }

    @Test
    void shouldReturnNullWhenCallingOnEmptyArray() {
        arrayDeque.clear();

        assertEquals(null, arrayDeque.poll());
    }

    @Test
    void shouldReturnFirstElement() {
        Student student = arrayDeque.getFirst();

        assertEquals(student, arrayDeque.element());
    }

    @Test
    void shouldReturnFirstElementIfArrayHaveElements() {
        assertFalse(arrayDeque.isEmpty());
        assertEquals(st6, arrayDeque.peek());
    }

    @Test
    void shouldMoveTailAndHeadOnPositionZeroIfArrayIsEmpty() {
        arrayDeque.clear();
        arrayDeque.push(st1);

        assertEquals(arrayDeque.getHead(), arrayDeque.getTail());
    }

    @Test
    void shouldReturnLastElementThatWasAdded() {
        arrayDeque.clear();
        arrayDeque.push(st1);

        assertEquals(st1, arrayDeque.pop());
    }

    @Test
    void shouldReturnTrueIfTheElementIsPresentInTheList() {
        assertTrue(arrayDeque.contains(st1));
    }

    @Test
    void shouldDecrementSizeIfAnElementIsRemoved() {
        int initialSize = arrayDeque.size();
        arrayDeque.pop();

        assertEquals(initialSize - 1, arrayDeque.size());

    }

    @Test
    void shouldGetFirstElementOfTheArray() {
        Iterator<Student> iter = arrayDeque.iterator();
        Student student = iter.next();

        assertSame(student, arrayDeque.getFirst());
    }

    @Test
    void shouldCheckIfTheLastElementHasNextAndReturnIt() {
        assertAll(
                () -> assertTrue(arrayDeque.descendingIterator().hasNext()),
                () -> assertEquals(arrayDeque.getLast(), arrayDeque.descendingIterator().next())
        );
    }

    @Test
    void shouldCheckIfArrayIsEmpty() {
        assertFalse(arrayDeque.isEmpty());
    }

    @Test
    void shouldCheckIfTheToArrayMethodCopiedElementsCorrectly() {
        Object[] student = arrayDeque.toArray();

        assertEquals(student[0], arrayDeque.getFirst());
        assertEquals(student[arrayDeque.getTail()], arrayDeque.getLast());
    }

    @Test
    void shouldCheckIfElementsFromArrayWasCopiedToArrayFromParameters() {
        Student[] studentArr = new Student[arrayDeque.getCapacity()];
        studentArr = arrayDeque.toArray(studentArr);

        assertSame(studentArr[0], arrayDeque.getFirst());
        assertSame(studentArr[arrayDeque.size() - 1], arrayDeque.getLast());
    }

    @Test
    void shouldCheckIfArrayDequeContainsAllElementsFromCollection() {
        assertFalse(arrayDeque.contains(Arrays.asList(st1, st2, st3, st4, st5, st6)));
    }

    @Test
    void shouldReturnTrueIfAllElementsFromGivenCollectionWereAddedToArrayDeque() {
        int initialSize = arrayDeque.size();
        assertAll(
                () -> assertTrue(arrayDeque.addAll(Arrays.asList(st1, st2, st3, st4))),
                () -> assertEquals(initialSize + 4, arrayDeque.size())
        );
    }

    @Test
    void shouldReturnTrueIfAtLeastOneElementWasDeletedFromArrayDeque() {
        assertTrue(arrayDeque.removeAll(Arrays.asList(st1, st2)));
    }

    @Test
    void shouldReturnTrueIfAtLeastOneElementWasRetain() {
        assertTrue(arrayDeque.retainAll(Arrays.asList(st1, st2, st3, st4, st5, st6)));
    }

    @Test
    void shoulMakeTheArrayEmpty() {
        arrayDeque.clear();

        assertTrue(arrayDeque.isEmpty());
    }
}