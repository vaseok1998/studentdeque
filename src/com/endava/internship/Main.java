package com.endava.internship;

import java.time.LocalDate;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Student st1 = new Student("vasea1", LocalDate.now(), "vasea1");
        Student st2 = new Student("vasea2", LocalDate.now(), "vasea2");
        Student st3 = new Student("vasea3", LocalDate.now(), "vasea3");
        Student st4 = new Student("vasea4", LocalDate.now(), "vasea4");
        Student st5 = new Student("vasea5", LocalDate.now(), "vasea5");
        Student st6 = new Student("vasea6", LocalDate.now(), "vasea6");
        Student st7 = new Student("vasea7", LocalDate.now(), "vasea7");

        ArrayDeque<Student> st = new ArrayDeque<>();

        st.add(st1);
        st.addFirst(st2);
        st.push(st3);
        st.addFirst(st4);
        st.addFirst(st4);
        st.addLast(st5);
        st.addLast(st7);
        st.add(st6);
        st.addFirst(st6);

        System.out.println("Initial ArrayDeque with inserted elements.");
        st.printElements();

        System.out.println();

        st.removeFirstOccurrence(st6);
        st.removeFirstOccurrence(st6);

        System.out.println("ArrayDeque after removing first and last occurrence of Vasea6");
        st.printElements();

        System.out.println();

        System.out.println(st.pollFirst().toString() + "  --- result of poolFirst()");
        System.out.println(st.peekFirst() + "  --- result of peekFirst()");
        System.out.println();

        System.out.println(st.retainAll(Arrays.asList(st1, st2, st3, st4, st7)) + "   --- result of retainAll() - Vasea1, Vasea2, Vasea3, Vasea4, Vasea7");
        System.out.println("ArrayDeque after the retainAll() was executed");
        st.printElements();

        System.out.println();
        System.out.println(st.removeAll(Arrays.asList(st7, st2)) + "   --- result of removeAll() - Vasea7, Vasea2");
        System.out.println("ArrayDeque after the removeAll() was executed");
        st.printElements();

        System.out.println();
        System.out.println(st.containsAll(Arrays.asList(st3, st1)) + "   --- result of containsAll() - Vasea3, Vasea1");
        st.printElements();

        System.out.println();
        System.out.println(st.removeFirstOccurrence(st3) + "   --- result of removeFirstOccurrence() - Vasea3");
        st.printElements();
        System.out.println();

        System.out.println(st.addAll(Arrays.asList(st1, st2, st3, st4, st5)) + "   --- result of addAll() - Vasea1, Vasea2, Vasea3, Vasea4, Vasea5");
        st.printElements();
        System.out.println();
    }
}
